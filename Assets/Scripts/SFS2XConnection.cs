﻿using UnityEngine;
using Sfs2X;

public class SFS2XConnection : MonoBehaviour
{
	private static SFS2XConnection mInstance; 
	private static SmartFox sfs;
	
	public static SmartFox Connection {
		get {
			if (mInstance == null) {
				mInstance = new GameObject("SFS2XConnection").AddComponent(typeof(SFS2XConnection)) as SFS2XConnection;
			}
			return sfs;
		}
		set {
			if (mInstance == null) {
				mInstance = new GameObject("SFS2XConnection").AddComponent(typeof(SFS2XConnection)) as SFS2XConnection;
			}
			sfs = value;
		} 
	}
	
	public static bool IsInitialized {
		get { 
			return (sfs != null); 
		}
	}
	
	void OnApplicationQuit() 
	{ 
		if (sfs.IsConnected) 
		{
			sfs.Disconnect();
		}
	} 
}