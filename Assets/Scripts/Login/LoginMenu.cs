﻿using System;
using UnityEngine;
using TMPro;

public class LoginMenu : MonoBehaviour
{
    public TMP_InputField Login;
    public TMP_InputField Password;

    public GameObject LoginMenuObj;
    public GameObject RegisterMenuObj;

    private SFS2X_Connect Connector;

    private void Start() 
    {
        Connector = GetComponentInParent<SFS2X_Connect>();
    }

    public void LoginClick()
    {
        if (Connector != null)
        {
            Connector.LoginInServ(Login.text, Password.text);
        }
        else
        {
            Debug.LogWarning("Connector not found");
        }
    }

    public void RegisterClick()
    {
        LoginMenuObj.SetActive(false);
        RegisterMenuObj.SetActive(true);
    }
}
