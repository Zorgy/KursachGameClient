﻿using System;
using UnityEngine;
using TMPro;

public class ConnectionMenu : MonoBehaviour
{
    public TMP_InputField IP;
    public TMP_InputField Port;

    public String DefaultIP = "192.168.0.102";
    public String DefaultPort = "9933";

    private SFS2X_Connect Connector;

    private void Start() 
    {
        Connector = GetComponentInParent<SFS2X_Connect>();

        IP.text = DefaultIP;
        Port.text = DefaultPort;
    }

    public void Connect()
    {
        if (Connector != null)
        {
            Connector.ConnectToServ(IP.text, Convert.ToInt32(Port.text));
        }
        else
        {
            Debug.LogWarning("Connector not found");
        }
    }
}
