﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Sfs2X;
using Sfs2X.Requests;
using Sfs2X.Util;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using TMPro;

public class SFS2X_Connect : MonoBehaviour
{
    public string Zone = "Kursach_game";

    public GameObject ConnectionMenu;
    public GameObject LoginMenu;

    public TextMeshProUGUI RegisterReasonText;
    public TextMeshProUGUI LoginReasonText;

    private SmartFox sfs;

    private bool registrationRequest = false;
    private ISFSObject regObj = new SFSObject();
    
    public void ConnectToServ(string Host, int Port)
    {
        Debug.Log("Now connecting...");

        sfs = new SmartFox();
        
        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.UDP_INIT, OnUdpInit);

        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);

        sfs.AddEventListener(SFSEvent.LOGOUT, OnLogout);

        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponce);

        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = Host;
        cfg.Port = Port;
        cfg.UdpHost = Host;
		cfg.UdpPort = 9934;
        cfg.Zone = Zone;
        cfg.Debug = true;
            
        // Connect to SFS2X
        sfs.Connect(cfg);
    }

    public void OnExitClicked()
    {
        Application.Quit();
    }

    public void Logout()
    {
        sfs.Send(new Sfs2X.Requests.LogoutRequest());
    }

    public void LoginInServ(string Login, string Password)
    {
        Debug.Log("SendLoginRequest");
        sfs.Send(new Sfs2X.Requests.LoginRequest(Login, PasswordUtil.MD5Password(Password)));
    }

    public void RegisterInServ(string Login, string Password)
    {
        Debug.Log("SendLoginRequest");
        sfs.Send(new Sfs2X.Requests.LoginRequest("Guest", ""));

        regObj.PutUtfString("login", Login);
        regObj.PutUtfString("password", PasswordUtil.MD5Password(Password));

        registrationRequest = true;
    }

    void Update() 
    {
		// As Unity is not thread safe, we process the queued up callbacks on every frame
		if (sfs != null)
			sfs.ProcessEvents();
	}

    private void OnExtensionResponce(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        ISFSObject objIn = (ISFSObject)evt.Params["params"];

        if (cmd == "register_user")
        {
            if (!objIn.GetBool("success"))
            {
                RegisterReasonText.text = objIn.GetUtfString("reason");
            }
            else
            {
                RegisterReasonText.color = new Color(35, 210, 37);
                RegisterReasonText.text = "Успешно";

            }
        }
    }

    private void OnLogin(BaseEvent evt)
    {
        if (registrationRequest)
        {
            sfs.Send(new ExtensionRequest("register_user", regObj));
            registrationRequest = false;
            Logout();
        }

        User inObj = (User)evt.Params["user"];
        if (inObj.PrivilegeId > 0)
        {
            sfs.InitUDP();
            SceneManager.LoadScene(1);
        }
    }

    private void OnLoginError(BaseEvent evt)
    {
        LoginReasonText.text = "Неверный логин/пароль";
    }

    private void OnLogout(BaseEvent evt)
    {
        Debug.Log("logout");
    }

    private void OnConnection(BaseEvent evt) {
		if ((bool)evt.Params["success"]) {
            SFS2XConnection.Connection = sfs;

			Debug.Log("Connection established successfully");
			Debug.Log("SFS2X API version: " + sfs.Version);
			Debug.Log("Connection mode is: " + sfs.ConnectionMode);

            ConnectionMenu.SetActive(false);
            LoginMenu.SetActive(true);

		} else {
			Debug.Log("Connection failed; is the server running at all?");
			
			// Remove SFS2X listeners and re-enable interface
			reset();
		}
    }
    private void OnUdpInit(BaseEvent evt) {
		Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);
		
		// Remove SFS2X listeners and re-enable interface
		reset();
	}
	
	private void OnConnectionLost(BaseEvent evt) {
		Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);
		
		// Remove SFS2X listeners and re-enable interface
		reset();
	}

    private void reset() {
		// Remove SFS2X listeners
		sfs.RemoveEventListener(SFSEvent.CONNECTION, OnConnection);
		sfs.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);

        sfs.RemoveEventListener(SFSEvent.LOGIN, OnLogin);
        sfs.RemoveEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);

        sfs.RemoveEventListener(SFSEvent.LOGOUT, OnLogout);

        sfs.RemoveEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponce);
		
		sfs = null;
	}
}
