﻿using UnityEngine;
using TMPro;


public class RegisterMenu : MonoBehaviour
{
    public TMP_InputField Login;
    public TMP_InputField Password;

    public GameObject LoginMenuObj;
    public GameObject RegisterMenuObj;

    private SFS2X_Connect Connector;

    private void Start() 
    {
        Connector = GetComponentInParent<SFS2X_Connect>();
    }

    public void OnRegisterClick()
    {
        if (Connector != null)
        {
            Connector.RegisterInServ(Login.text, Password.text);
        }
        else
        {
            Debug.LogWarning("Connector not found");
        }
    }

    public void OnBackClick()
    {
        if (Connector != null)
        {
            LoginMenuObj.SetActive(true);
            RegisterMenuObj.SetActive(false);
            Connector.Logout();
        }
        else
        {
            Debug.LogWarning("Connector not found");
        }
    }
}
