﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Sfs2X;

public class PlayerHUD : MonoBehaviour
{
    public void OnLobbyClicked()
    {
        SmartFox sfs = SFS2XConnection.Connection;

        sfs.Send(new Sfs2X.Requests.LeaveRoomRequest());
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
