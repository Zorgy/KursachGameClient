﻿using System.Collections;
using System.Collections.Generic;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using UnityEngine;

public class GamePlayer : MonoBehaviour
{
    public User user;
    public bool died = false;

    private GameObject PlayerGameObject;
    private TransformInterpolation interpolator;
    private Actions actions;
    private AnimSynchronizer animSync;
    public PlayerController pc;
    private EyeCameraController ecc;
    
    public void SpawnPlayer(GameObject ClonningObj)
    {
        PlayerGameObject = Instantiate(ClonningObj);
        interpolator = PlayerGameObject.GetComponent<TransformInterpolation>();
        actions = PlayerGameObject.GetComponent<Actions>();
        animSync = PlayerGameObject.GetComponent<AnimSynchronizer>();
        pc = PlayerGameObject.GetComponent<PlayerController>();
        ecc = PlayerGameObject.GetComponent<EyeCameraController>();
        if (pc != null)
        {
            pc.id = user.Id;
        }

        if (interpolator != null)
        {
            interpolator.StartReceiving();
        }
    }

    public void UpdateHP(int NewHP)
    {
        actions.UpdateHP(NewHP);
    }

    public void KillEnemy()
    {
        actions.Death();
        actions.enabled = false;
        died = true;
    }

    public void KillYourself()
    {
        actions.Death();
        actions.enabled = false;
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        pc.HUD.transform.Find("Died").gameObject.SetActive(true);
        pc.enabled = false;
        ecc.enabled = false;
    }

    public void SetTransformFromSFSObj(ISFSObject obj)
    {
        float x = (float) obj.GetDouble("x");
        float y = (float) obj.GetDouble("y");
        float z = (float) obj.GetDouble("z");

        PlayerGameObject.transform.position = new Vector3(x,y,z);

        float rotx = (float) obj.GetDouble("rx");
        float roty = (float) obj.GetDouble("ry");
        float rotz = (float) obj.GetDouble("rz");

        transform.localEulerAngles = new Vector3(rotx, roty, rotz);
    }

    public void InterpolateTransformFromSFSObj(ISFSObject obj, double timestamp)
    {
        float x = (float) obj.GetDouble("x");
        float y = (float) obj.GetDouble("y");
        float z = (float) obj.GetDouble("z");

        Vector3 pos = new Vector3(x,y,z);

        float rotx = (float) obj.GetDouble("rx");
        float roty = (float) obj.GetDouble("ry");
        float rotz = (float) obj.GetDouble("rz");

        Vector3 rot = new Vector3(rotx, roty, rotz);

        interpolator.ReceivedTransform(pos, Quaternion.Euler(rot), timestamp);
    }


    public ISFSObject GetTransformSFSObj()
    {
        ISFSObject obj = new SFSObject();
        obj.PutDouble("x", PlayerGameObject.transform.position.x);
        obj.PutDouble("y", PlayerGameObject.transform.position.y);
        obj.PutDouble("z", PlayerGameObject.transform.position.z);

        obj.PutDouble("rx", PlayerGameObject.transform.localEulerAngles.x);
        obj.PutDouble("ry", PlayerGameObject.transform.localEulerAngles.y);
        obj.PutDouble("rz", PlayerGameObject.transform.localEulerAngles.z);

        ISFSObject transformObj = new SFSObject();
        transformObj.PutSFSObject("transform", obj);

        return transformObj;
    }

    public void PlayAnim(string state, double timestamp)
    {
        animSync.AddState(state, timestamp);
    }

    public void ChangeWeapon(string weapon, double timestamp)
    {
        animSync.AddWeaponReq(weapon, timestamp);
    }
}
