﻿using System;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Requests;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Variables;
using Sfs2X.Entities.Data;
using UnityEngine.SceneManagement;
using UnityEngine;


public class NetworkController : MonoBehaviour
{
    private static NetworkController instance;
	public static NetworkController Instance {
		get {
			return instance;
		}
	}
    public GameObject MainPlayer;
    public GameObject OtherPlayer;

    private SmartFox sfs;
    private Room room;
    
    public Dictionary<int, GamePlayer> UserList = new Dictionary<int, GamePlayer>();

    public GamePlayer Me {
        get {
            return UserList[sfs.MySelf.Id];
        }
    }

    void Awake() {
		instance = this;	
	}

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        sfs = SFS2XConnection.Connection;

        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponce);

        room = sfs.LastJoinedRoom;

        TimeController.Instance.Init();

        sfs.Send(new ExtensionRequest("spawn_me", new SFSObject(), room));
    }

    void Update()
    {
        if (sfs == null)
        {
            SceneManager.LoadScene(0);
        }
        
        if (sfs != null)
			sfs.ProcessEvents();
    }

    public void SendAnimation(string state) 
    {
        ISFSObject AnimationObj = new SFSObject();
        AnimationObj.PutUtfString("animation", state);

        sfs.Send(new ExtensionRequest("send_animation", AnimationObj, room));
    }

    public void TimeSyncRequest() 
    {
		sfs.Send(new ExtensionRequest("get_time", new SFSObject(), room));
	}

    public void SendTransform(ISFSObject TransformObj)
    {
        sfs.Send(new ExtensionRequest("send_transform", TransformObj, room, true));
    }

    public void SendShot(int playerId)
    {
        Debug.Log("sendShot");
        ISFSObject obj = new SFSObject();
        obj.PutInt("id", playerId);

        sfs.Send(new ExtensionRequest("bullet_shot", obj, room));
    }

    public void ChangeWeapon(string weapon)
    {
        Debug.Log("Weapon change: " + weapon);
        ISFSObject WeaponObj = new SFSObject();
        WeaponObj.PutUtfString("weapon", weapon);

        sfs.Send(new ExtensionRequest("change_weapon", WeaponObj, room));
    }

    private void reset()
    {
		sfs.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
		
		sfs = null;
	}

    private void OnExtensionResponce(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        ISFSObject objIn = (ISFSObject)evt.Params["params"];

        switch (cmd)
        {
            case "spawn_player":
            InstantiatePlayer(objIn);
            break;

            case "send_transform":
            ChangeTransform(objIn);
            break;

            case "time":
            HandleServerTime(objIn);
            break;

            case "send_animation":
            HandleAnimation(objIn);
            break;

            case "change_weapon":
            ChangeWeaponHandler(objIn);
            break;

            case "kill_user":
            KillUserHandler(objIn);
            break;

            case "update_hp":
            UpdateHPHandler(objIn);
            break;

            default:
            break;
        }
    }

    private void UpdateHPHandler(ISFSObject data) {
		int NewHP = data.GetInt("HP");

		UserList[sfs.MySelf.Id].UpdateHP(NewHP);
	}

    private void KillUserHandler(ISFSObject data) {

		ISFSObject player = data.GetSFSObject("player");

        if (player != null)
        {
            int id = player.GetInt("id");

            if (id == sfs.MySelf.Id)
            {
                UserList[id].KillYourself();
            }
            else
            {
                Debug.Log("kill player" + id.ToString());
                UserList[id].KillEnemy();
            }
        }

        int deathsCount = 0;
        foreach (int playerID in UserList.Keys)
        {
            if (playerID != Me.user.Id && UserList[playerID].died)
            {
                deathsCount++;
            }
        }
        
        if (deathsCount >= UserList.Count - 1 && !UserList[Me.user.Id].died)
        {
            
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            UserList[Me.user.Id].pc.HUD.transform.Find("Wictory").gameObject.SetActive(true);
        }
	}

    private void ChangeWeaponHandler(ISFSObject data) {
		int id = data.GetInt("id");
        string weapon = data.GetUtfString("weapon");
        double Timestamp = Convert.ToDouble(data.GetLong("timestamp"));

        if (id != sfs.MySelf.Id)
        {
		    UserList[id].ChangeWeapon(weapon, Timestamp);
        }
	}

    private void HandleAnimation(ISFSObject data) {
		int id = data.GetInt("id");
        string state = data.GetUtfString("animation");
        double Timestamp = Convert.ToDouble(data.GetLong("timestamp"));

        if (id != sfs.MySelf.Id)
        {
		    UserList[id].PlayAnim(state, Timestamp);
        }

	}

    private void HandleServerTime(ISFSObject dt) {
		long time = dt.GetLong("t");
		TimeController.Instance.Synchronize(Convert.ToDouble(time));
	}

    private void ChangeTransform(ISFSObject obj)
    {
        ISFSObject player = obj.GetSFSObject("player");

        int id = obj.GetInt("id");

        if (id == sfs.MySelf.Id)
        {
           return;
        }
        else
        {
            double Timestamp = Convert.ToDouble(obj.GetLong("timestamp"));
            UserList[id].InterpolateTransformFromSFSObj(obj.GetSFSObject("transform"), Timestamp);
        }
    }

    private void InstantiatePlayer(ISFSObject obj)
    {
        ISFSObject player = obj.GetSFSObject("player");

        if (player != null)
        {
            int id = player.GetInt("id");

            GamePlayer gp = gameObject.AddComponent(typeof(GamePlayer)) as GamePlayer;

            gp.user = sfs.UserManager.GetUserById(id);

            UserList.Add(id, gp);

            if (id == sfs.MySelf.Id)
            {
                gp.SpawnPlayer(MainPlayer);
            }
            else
            {
                gp.SpawnPlayer(OtherPlayer);
            }

            double Timestamp = Convert.ToDouble(obj.GetLong("timestamp"));
            gp.SetTransformFromSFSObj(player.GetSFSObject("transform"));

        }
        else
        {
            Debug.Log("invalid player obj");
        }

    }

    private void OnConnectionLost(BaseEvent evt) 
    {
		Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);
		
		reset();
	}
}
