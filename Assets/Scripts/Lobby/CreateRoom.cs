﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CreateRoom : MonoBehaviour
{
    
    public TMP_InputField RoomName;
    public TMP_Dropdown MapNames;

    public CanvasGroup MainMenuObj;
    public GameObject CreateRoomObj;

    SFS2X_Lobby Lobby;

    private void Awake()
    {
        Lobby = GetComponentInParent<SFS2X_Lobby>();

        foreach (string map in Maps.MapsNames)
        {
            MapNames.options.Add(new TMP_Dropdown.OptionData() {text = map});
        }
        MapNames.value = 0;
    }
    public void OnCreateNewClicked()
    {
        MainMenuObj.interactable = false;
        CreateRoomObj.SetActive(true);
    }

    public void OnSubmitClicked()
    {
        MainMenuObj.interactable = true;
        CreateRoomObj.SetActive(false);

        string room = RoomName.text;
        string map = MapNames.captionText.text;

        Lobby.AddRoom(room, map);
    }
}
