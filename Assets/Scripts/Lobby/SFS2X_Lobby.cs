﻿using System;
using System.Collections.Generic;
using Sfs2X;
using Sfs2X.Requests;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Variables;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SFS2X_Lobby : MonoBehaviour
{
    public TMP_Text chatText;
    public TMP_Text userListText;
    public ScrollRect chatScrollView;
    public TMP_InputField msgField;
	public Transform RoomList;
	public TMP_Dropdown RoomMapName;
	public GameObject RoomListItem;
	public GameObject RoomCanvas;
	public GameObject LobbyChatCanvas;

    private static SmartFox sfs;
    private bool firstJoin = true;
    private Room currRoom;
	
    void Awake()
    {
        sfs = SFS2XConnection.Connection;

        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
		sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
		sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);
		sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnUserEnterRoom);
		sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnUserExitRoom);
		sfs.AddEventListener(SFSEvent.ROOM_ADD, OnRoomAdd);
		sfs.AddEventListener(SFSEvent.ROOM_VARIABLES_UPDATE, OnVarsUpdate);

		Debug.Log("Joining Lobby");
        sfs.Send(new Sfs2X.Requests.JoinRoomRequest("Lobby"));
    }

    void Update()
    {
        if (sfs == null)
        {
            SceneManager.LoadScene(0);
        }
        
        if (sfs != null)
			sfs.ProcessEvents();
    }

    public void OnSendMessageButtonClick() {
		if (msgField.text != "") {
			// Send public message to Room
			sfs.Send (new Sfs2X.Requests.PublicMessageRequest(msgField.text));

			// Reset message field
			msgField.text = "";
		}

		msgField.ActivateInputField();
		msgField.Select();
	}

    public void OnExitClicked()
    {
        if (currRoom.Name != "Lobby")
        {
            sfs.Send(new Sfs2X.Requests.LeaveRoomRequest());
            sfs.Send(new Sfs2X.Requests.JoinRoomRequest("Lobby"));
        }
        else
        {
            Application.Quit();
        }
    }

	public void OnRoomItemClick(int roomId) {

		sfs.Send(new Sfs2X.Requests.JoinRoomRequest(roomId));
	}

	public void AddRoom(string RoomName, string RoomMap)
    {
        RoomSettings settings = new RoomSettings(RoomName);
        settings.IsGame = true;
        settings.MaxUsers = 2;

        List<RoomVariable> varLst = new List<RoomVariable>();
        RoomVariable var = new SFSRoomVariable("RoomMap", RoomMap)
        {
            IsPersistent = false,
            IsPrivate = false
        };
        varLst.Add(var);

        settings.Variables = varLst;

		settings.Extension = new RoomExtension("kursachGameRoomExtension", "kursachGameRoomExtension.KursachGameRoomExtension");

        sfs.Send(new Sfs2X.Requests.CreateRoomRequest(settings, true, currRoom));
    }

	public void OnStartGameClicked()
	{
		List<RoomVariable> varLst = new List<RoomVariable>();
        RoomVariable var = new SFSRoomVariable("GameStarted", true)
        {
            IsPersistent = true,
            IsPrivate = false
        };
		varLst.Add(var);

		sfs.Send(new Sfs2X.Requests.SetRoomVariablesRequest(varLst));
	}

	public void OnChangeRoomMap(int value)
	{
		string NewRoomName = RoomMapName.captionText.text;
		List<RoomVariable> varLst = new List<RoomVariable>();
        RoomVariable var = new SFSRoomVariable("RoomMap", NewRoomName)
        {
            IsPersistent = false,
            IsPrivate = false
        };
		varLst.Add(var);

		sfs.Send(new Sfs2X.Requests.SetRoomVariablesRequest(varLst));
	}

	private void populateRoomsList() {
		List<Room> rooms = sfs.RoomManager.GetRoomList();
		
		foreach (Transform child in RoomList.transform) {
			GameObject.Destroy(child.gameObject);
		}

		foreach (Room room in rooms) {

			if (!room.IsGame || room.IsHidden || room.IsPasswordProtected) {
				continue;
			}	

			int roomId = room.Id;

			GameObject newListItem = Instantiate(RoomListItem) as GameObject;
			RoomItem roomItem = newListItem.GetComponent<RoomItem>();
			roomItem.nameLabel.text = room.Name;
			roomItem.roomId = roomId;

			roomItem.button.onClick.AddListener(() => OnRoomItemClick(roomId));

			newListItem.transform.SetParent(RoomList, false);
		}
	}

    private void populateUserList(List<User> users) {
		List<string> userNames = new List<string>();

		foreach (User user in users) {

			string name = user.Name;

			if (user == sfs.MySelf)
				name += " <color=#808080ff>(you)</color>";

            Debug.Log(name);
			userNames.Add(name);
		}

		userNames.Sort();

		userListText.text = "";
		userListText.text = String.Join("\n", userNames.ToArray());
	}

	private void changeRoomCanvas()
	{
		if (currRoom.Name == "Lobby")
		{
			RoomCanvas.SetActive(false);
			LobbyChatCanvas.SetActive(true);

			Transform ChatArea = LobbyChatCanvas.transform.Find("ChatArea");
			Transform Text = LobbyChatCanvas.transform.Find("ChatArea/Viewport/ChatContent");
			Transform Message = LobbyChatCanvas.transform.Find("Actions/MessageInput");

			chatScrollView = ChatArea.gameObject.GetComponent<ScrollRect>();
			chatText = Text.gameObject.GetComponent<TMP_Text>();
			msgField = Message.gameObject.GetComponent<TMP_InputField>();
		}
		else
		{
			RoomCanvas.SetActive(true);
			LobbyChatCanvas.SetActive(false);
			
			Transform ChatArea = RoomCanvas.transform.Find("RoomChat/ChatArea");
			Transform Text = RoomCanvas.transform.Find("RoomChat/ChatArea/Viewport/ChatContent");
			Transform Message = RoomCanvas.transform.Find("RoomChat/Actions/MessageInput");

			chatScrollView = ChatArea.gameObject.GetComponent<ScrollRect>();
			chatText = Text.gameObject.GetComponent<TMP_Text>();
			msgField = Message.gameObject.GetComponent<TMP_InputField>();

			string MapName = (string) currRoom.GetVariable("RoomMap").Value;

			int i = 1;
			RoomMapName.ClearOptions();
			foreach (string map in Maps.MapsNames)
			{
				RoomMapName.options.Add(new TMP_Dropdown.OptionData() {text = map});
				if (map == MapName)
				{			
					RoomMapName.value = i;
				}
				i++;
			}
		}
	}

    private void reset()
    {
		// Remove SFS2X listeners
		sfs.RemoveEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.RemoveEventListener(SFSEvent.ROOM_JOIN, OnRoomJoin);
		sfs.RemoveEventListener(SFSEvent.ROOM_JOIN_ERROR, OnRoomJoinError);
		sfs.RemoveEventListener(SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);
		sfs.RemoveEventListener(SFSEvent.USER_ENTER_ROOM, OnUserEnterRoom);
		sfs.RemoveEventListener(SFSEvent.USER_EXIT_ROOM, OnUserExitRoom);
		sfs.RemoveEventListener(SFSEvent.ROOM_ADD, OnRoomAdd);
		sfs.RemoveEventListener(SFSEvent.ROOM_VARIABLES_UPDATE, OnVarsUpdate);
		
		sfs = null;
        userListText.text = "";
		chatText.text = "";
	}

    private void printSystemMessage(string message) {
		chatText.text += "<color=#808080ff>" + message + "</color>\n";

		Canvas.ForceUpdateCanvases();

		chatScrollView.verticalNormalizedPosition = 0;
	}
	
	private void printUserMessage(User user, string message) {
		chatText.text += "<b>" + (user == sfs.MySelf ? "You" : user.Name) + ":</b> " + message + "\n";

		Canvas.ForceUpdateCanvases();

		chatScrollView.verticalNormalizedPosition = 0;
	}

    private void OnConnectionLost(BaseEvent evt) 
    {
		Debug.Log("Connection was lost; reason is: " + (string)evt.Params["reason"]);
		
		// Remove SFS2X listeners and re-enable interface
		reset();
	}

    private void OnRoomJoin(BaseEvent evt) 
    {
		Room room = (Room) evt.Params["room"];

		if (!firstJoin)
			chatText.text = "";

		firstJoin = false;

		Debug.Log("\nYou joined room '" + room.Name + "'\n");

        currRoom = room;
        populateUserList(room.UserList);
		populateRoomsList();
		changeRoomCanvas();
	}
	
	private void OnRoomJoinError(BaseEvent evt) 
    {
		printSystemMessage("Room join failed: " + (string) evt.Params["errorMessage"]);
	}
	
	private void OnPublicMessage(BaseEvent evt) 
    {
		User sender = (User) evt.Params["sender"];
		string message = (string) evt.Params["message"];

		printUserMessage(sender, message);
	}
	
	private void OnUserEnterRoom(BaseEvent evt) 
    {
		User user = (User) evt.Params["user"];
		Room room = (Room) evt.Params["room"];

		printSystemMessage("User " + user.Name + " entered the room");

        populateUserList(room.UserList);
		populateRoomsList();
	}
	
	private void OnUserExitRoom(BaseEvent evt) 
    {
		User user = (User) evt.Params["user"];

		if (user != sfs.MySelf) {
			Room room = (Room)evt.Params["room"];
			
			printSystemMessage("User " + user.Name + " left the room");

            populateUserList(room.UserList);
		}
		populateRoomsList();
	}

	private void OnRoomAdd(BaseEvent evt) 
    {
		populateRoomsList();
	}

	private void OnVarsUpdate(BaseEvent evt) 
    {
		Debug.Log("Room variables has changed");

		Room room = (Room) evt.Params["room"];
		currRoom = room;

		List<string> changedVars = (List<string>)evt.Params["changedVars"];
		if (changedVars.Find(x => x == "RoomMap") != null)
		{
			string MapName = (string) room.GetVariable("RoomMap").Value;

			int i = 0;
			foreach (string map in Maps.MapsNames)
			{
				if (map == MapName)
				{			
					RoomMapName.value = i;
				}
				i++;
			}
		}

		if (changedVars.Find(x => x == "GameStarted") !=  null)
		{
			bool GameStarted = (bool) room.GetVariable("GameStarted").Value;

			if (GameStarted)
			{
				SceneManager.LoadScene((string)currRoom.GetVariable("RoomMap").Value);
			}
		}
	}
}
