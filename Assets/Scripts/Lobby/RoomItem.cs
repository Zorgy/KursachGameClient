﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RoomItem : MonoBehaviour
{
    public Button button;
	public TMP_Text nameLabel;

	public int roomId;
}
