﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    public float HP = 100.0f;

    private Animator anim;
    private PlayerController pc;
    private Actions actions;
    private EyeCameraController ecc;
    private void Start() 
    {
        anim = GetComponent<Animator>();
        pc = GetComponent<PlayerController>();
        actions = GetComponent<Actions>();
        ecc = GetComponent<EyeCameraController>();
    }

    void Update()
    {
        if (HP <= 0.0f)
        {
            anim.SetTrigger("Death");
            pc.enabled = false;
            actions.enabled = false;
            ecc.enabled = false;
        }
    }
}
