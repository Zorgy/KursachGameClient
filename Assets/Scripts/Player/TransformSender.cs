﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformSender : MonoBehaviour
{
	public static float sendingPeriod = 0.03f; 
	private float timeLastSending = 0.0f;
	private bool send = false;

    private NetworkController nc;
	private Transform thisTransform;
	
	void Start() {
		thisTransform = this.transform;
        StartSendTransform();
	}
		
	void StartSendTransform() {
		send = true;
        nc = NetworkController.Instance;
	}
	
	void FixedUpdate() { 
		if (send) {
			SendTransform();
		}
	}
	
	void SendTransform() {
        if (timeLastSending >= sendingPeriod) {
            nc.SendTransform(nc.Me.GetTransformSFSObj());
            timeLastSending = 0;
            return;
        }

		timeLastSending += Time.deltaTime;
	}
}
