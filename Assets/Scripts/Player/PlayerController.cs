using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public static float sendingPeriod = 0.5f; 

    public float JumpForce = 100.0f;
    public float Speed = 4.4f;
    public int id;
    public GameObject HUD;

    public Animator animator;
    public Actions actions;
    public Collider footCollider;

    private bool canSendAnim = true;
    private Rigidbody rb;
    private NetworkController nc;
    private float distToGround;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        actions = GetComponent<Actions>();
        nc = NetworkController.Instance;
	}

    private void Start() {
        distToGround = footCollider.bounds.extents.y;
    }

    private void Update() {

        //Jump();

        if (Input.GetButtonDown("Vertical"))
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
            {
                actions.WalkAnim(true);
                nc.SendAnimation("is_walking");
            }
        }

        if (Input.GetButtonUp("Vertical"))
        {
            if (canSendAnim)
            {
                actions.WalkAnim(false);
                nc.SendAnimation("is_walking_stop");
            }
        }

        if (Input.GetButtonDown("Weapon1")) 
        {
            actions.SetWeapon("Rifle");
            nc.ChangeWeapon("Rifle");
        }

        if (Input.GetButtonDown("Weapon0"))
        {
            actions.SetWeapon("None");
            nc.ChangeWeapon("None");
        }

        if (Input.GetButtonDown("Run"))
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Run"))
            {
                actions.RunAnim(true);
                nc.SendAnimation("is_running");
            }
        }

        if (Input.GetButtonUp("Run"))
        {
            if (canSendAnim)
            {
                actions.RunAnim(false);
                nc.SendAnimation("is_running_stop");
            }
        }

        if (Input.GetButtonDown("Squat"))
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Sneak"))
            {
                actions.SquatAnim(true);
                nc.SendAnimation("Squat");
            }
        }

        if (Input.GetButtonUp("Squat"))
        {
            if (canSendAnim)
            {
                actions.SquatAnim(false);
                nc.SendAnimation("Squat_stop");
            }
        }

        if (Input.GetButton("Fire"))
        { 
            if (canSendAnim && actions.canFire)
            {
                actions.Fire();
                nc.SendAnimation("Fire");
            }
        }
    }
    public void Jump()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.AddForce(new Vector3(0, JumpForce, 0), ForceMode.Impulse);
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
            {
                actions.JumpAnim();
                nc.SendAnimation("Jump");
            }
        }
    }

    private void FixedUpdate() {
        Move();
    }

    IEnumerator UpdateGround()
    {
        yield return new WaitForSeconds(1f);

        distToGround = footCollider.bounds.extents.y;
    }


    private void Move()
    {
        float k = 1.0f;

        float horizontal = Input.GetAxis("Horizontal");
        
        float vertical = Input.GetAxis("Vertical");

        if (Input.GetButton("Run"))
            k *= 1.5f;

        Vector2 input = new Vector2(horizontal, vertical);

        Vector3 desiredMove = transform.forward * input.y + transform.right * input.x;
        Vector3 moveDir = new Vector3(desiredMove.x * Speed * k, 0, desiredMove.z * Speed * k);

        rb.velocity = moveDir;
    }

    private bool IsGrounded()
    {
         return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }
}
