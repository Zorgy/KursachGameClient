﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionCheck : MonoBehaviour
{
    public bool IsPlayerBullet;
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (!IsPlayerBullet)
        {
            if (collision.gameObject.tag == "Player")
            {
                PlayerController player = collision.gameObject.GetComponentInParent<PlayerController>();
                NetworkController.Instance.SendShot(player.id);
            }
        }
        Destroy(gameObject);
    }
}
