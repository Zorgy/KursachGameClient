﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeCameraController : MonoBehaviour
{
    public Transform target;

    public float VerticalLimit;

    private Transform character;
    private float yaw = 0.0f;
    private float pitch = 0.0f;
    
    void Awake() 
    {
        character = GetComponent<Transform>();
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        pitch += Input.GetAxis("Mouse Y");
        yaw += Input.GetAxis("Mouse X");

        if (Mathf.Abs(pitch) > VerticalLimit)
        {
            pitch = pitch > 0? VerticalLimit : -VerticalLimit;
        }
        
        target.localEulerAngles = new Vector3(0, 0, pitch);
        character.localEulerAngles = new Vector3(0, yaw, 0);
    }
}
