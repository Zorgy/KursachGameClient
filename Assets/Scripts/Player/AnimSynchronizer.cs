﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimSynchronizer : MonoBehaviour
{
    private List<AnimationWithTimestamp> anim_states = new List<AnimationWithTimestamp>();
    private List<WeaponsWithTimestamp> change_weapon_req = new List<WeaponsWithTimestamp>();

    private Actions actions;

    struct AnimationWithTimestamp
    {
        public string state;
        public double TimeStamp;
    }

    struct WeaponsWithTimestamp
    {
        public string weapon;
        public double TimeStamp;
    }

    TransformInterpolation interpolator;
    
    public void AddState(string State, double TimeStamp)
    {
        anim_states.Add(new AnimationWithTimestamp(){
            state = State,
            TimeStamp = TimeStamp
        });

        if (anim_states.Count > 100)
        {
            anim_states.RemoveAt(0);
        }
    }

    public void AddWeaponReq(string Weapon, double TimeStamp)
    {
        change_weapon_req.Add(new WeaponsWithTimestamp(){
            weapon = Weapon,
            TimeStamp = TimeStamp
        });

        if (change_weapon_req.Count > 15)
        {
            anim_states.RemoveAt(0);
        }
    }

    void Start()
    {
        interpolator = GetComponent<TransformInterpolation>();
        actions = GetComponent<Actions>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (AnimationWithTimestamp anim in anim_states)
        {
            if (anim.TimeStamp <= interpolator.CurrentInterpolatorTime)
            {
                actions.PlayAnim(anim.state);
                anim_states.Remove(anim);
                break;
            }
        }

        foreach (WeaponsWithTimestamp weapon in change_weapon_req)
        {
            if (weapon.TimeStamp <= interpolator.CurrentInterpolatorTime)
            {
                actions.SetWeapon(weapon.weapon);
                change_weapon_req.Remove(weapon);
                break;
            }
        }
    }
}
