﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponArsenal : MonoBehaviour
{
    [System.Serializable]
	public struct Arsenal {
		public string name;
        public float velocity;
		public GameObject Gun;
		public GameObject Bullet;
		public float FireRate;
		public RuntimeAnimatorController controller;
		public float BulletForce;
		public float Damage;
	}

    public Transform rightGunBone;
	public Transform leftGunBone;
    public Arsenal[] arsenal;

    private Animator animator;
	private Arsenal currentArsenal;

    void Awake()
    {
		animator = GetComponent<Animator>();
		if (arsenal.Length > 0)
			SetArsenal(arsenal[0].name);
    }
	public Arsenal GetCurrentArsenal()
	{
		return currentArsenal;
	}

    public void SetArsenal(string name) {
		foreach (Arsenal hand in arsenal) {
			if (hand.name == name) {
				if (rightGunBone.childCount > 0)
                {
					Destroy(rightGunBone.GetChild(0).gameObject);
                }

				if (leftGunBone.childCount > 0)
                {
					Destroy(leftGunBone.GetChild(0).gameObject);
                }

				if (hand.Gun != null)
                {
					GameObject newRightGun = (GameObject) Instantiate(hand.Gun);
					currentArsenal.Gun = newRightGun;
					newRightGun.transform.parent = rightGunBone;
					newRightGun.transform.localPosition = Vector3.zero;
					newRightGun.transform.localRotation = Quaternion.Euler(90, 0, 0);

					
				}

				animator.runtimeAnimatorController = hand.controller;

				currentArsenal.name = name;
				currentArsenal.Bullet = hand.Bullet;
				currentArsenal.FireRate = hand.FireRate;
				currentArsenal.BulletForce = hand.BulletForce;
				return;
				}
		}
	}
}
