﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Actions : MonoBehaviour
{
    public GameObject character;
    public Transform Body;
    public bool canFire = true;
    public TMP_Text HPText;

    private float HP = 100;
    private Animator anim;
    WeaponArsenal arsenal;
    private float FireRate;

    void Start()
    {
        anim = GetComponent<Animator>();
        arsenal = GetComponent<WeaponArsenal>();
    }

    public void UpdateHP(int NewHP)
    {
        HP = NewHP;
        HPText.text = NewHP.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        FireRate = arsenal.GetCurrentArsenal().FireRate;
    }

    public void PlayAnim(string state)
    {
        if (state == "is_walking")
           WalkAnim(true);

        if (state == "is_walking_stop")
            WalkAnim(false);

        if (state == "Jump")
            JumpAnim();
        
        if (state == "is_running")
            RunAnim(true);

        if (state == "is_running_stop")
            RunAnim(false);

        if (state == "Squat")
            SquatAnim(true);

        if (state == "Squat_stop")
            SquatAnim(false);
        
        if (state == "Fire")
            Fire();
    }

    public void WalkAnim(bool active)
    {
        anim.SetBool("is_walking", active);
    }

    public void JumpAnim()
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Jump"))
        {
            anim.SetTrigger("Jump");
        }

    }

    public void RunAnim(bool active)
    {
        anim.SetBool("is_running", active);
    }
    public void SquatAnim(bool active)
    {
        anim.SetBool("Squat", active);
    }

    IEnumerator FireRateCorutine()
    {
        yield return new WaitForSeconds(FireRate);

        canFire = true;
    }

    public void Fire()
    {
        if  (arsenal.GetCurrentArsenal().name != "Rifle") return;

        if (!canFire) return;
        canFire = false;
        StartCoroutine(FireRateCorutine());

        anim.SetTrigger("Fire");

        GameObject Gun = arsenal.GetCurrentArsenal().Gun;
        if (Gun == null)
        {
            Debug.Log("Gun is NULL");
            return;
        }
        
        GameObject BulletObj = arsenal.GetCurrentArsenal().Bullet;
        if (BulletObj == null)
        {
            Debug.Log("Bullet is NULL");
            return;
        }
        
        Transform GunPointTransform = Gun.transform.Find("GunPoint");

        if (BulletObj == null)
        {
            Debug.Log("Cannot find GunPoint");
            return;
        }

        GameObject Bullet =  (GameObject)Instantiate(BulletObj);
        Bullet.transform.position = GunPointTransform.transform.position;
        Bullet.transform.rotation = GunPointTransform.transform.rotation;

        Rigidbody bulletRb = Bullet.GetComponent<Rigidbody>();

        bulletRb.AddForce(Bullet.transform.forward * arsenal.GetCurrentArsenal().BulletForce, ForceMode.Impulse);
    }

    public void SetWeapon(string name)
    {
        arsenal.SetArsenal(name);
    }
    public void Death()
    {
        anim.SetTrigger("Death");
    }
}